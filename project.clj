(defproject hps-graphview "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[heuristic-problem-solver "1.0.0-SNAPSHOT"]]
  :java-source-paths ["src/java"]
  :javac-options ["-Xlint:deprecation,unchecked"]
  :prep-tasks [["compile" "hps.types"]
               "javac" "compile"])
