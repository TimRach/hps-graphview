import java.util.HashMap;
import java.util.Iterator;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.shape.CubicCurve;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;

import clojure.lang.PersistentArrayMap;
import clojure.lang.Keyword;
import clojure.lang.Range;

public class GraphNode extends StackPane implements IConnectable {

    private Label title;

    private HashMap<Keyword,Label> parameterLabels = new HashMap<Keyword,Label>();
    private int nodeHeight = 10;

    public ArrayList<CubicCurve> inConnections = new ArrayList<CubicCurve>();
    public ArrayList<CubicCurve> outConnections = new ArrayList<CubicCurve>();

    public GraphNode(String _title, NodeType type, Object parameters) {
        if(parameters != null){
            Object[] pkeys = ((PersistentArrayMap)parameters).keySet().toArray();
            Object[] pvals = ((PersistentArrayMap)parameters).values().toArray();
            for(int i = 0; i < pkeys.length; i++){
                String value = ((Keyword)pkeys[i]).getName() + ": " + internalToString(pvals[i]);
                Label plabel = new Label(value);
                parameterLabels.put((Keyword)pkeys[i], plabel);
            }
            nodeHeight = nodeHeight + 25 * pkeys.length;
        }

        String stylePrefix = "graphnode";
        //Make TitlePane
        Pane titlePane = new Pane();
        titlePane.setMinWidth(200);
        Label typeChar = new Label("-");
        switch (type) {
        case PRODUCER: typeChar.setText("P"); stylePrefix = "expert";break;
        case EVALUATOR: typeChar.setText("E"); stylePrefix = "expert";break;
        case GENERIC: typeChar.setText(""); stylePrefix = "input";break;
        }
        typeChar.setTranslateX(7);
        typeChar.setTranslateY(2);
        typeChar.getStyleClass().add(stylePrefix + "-type");

        title = new Label(_title);
        title.setTextFill(Color.BLACK);
        title.setTextAlignment(TextAlignment.CENTER);
        title.setTranslateX(25);
        title.setTranslateY(2);
        title.getStyleClass().add(stylePrefix + "-title");

        titlePane.getChildren().add(typeChar);
        titlePane.getChildren().add(title);

        //Make ContentPane
        Pane contentPane = new Pane();
        contentPane.setMinHeight(nodeHeight);
        contentPane.setTranslateY(20);

        int counter = 0;
        for(Label l: parameterLabels.values()){
            l.getStyleClass().add(stylePrefix + "-content-label");
            l.setTranslateX(10);
            l.setTranslateY(5 + (25 * counter));
            counter++;
        }

        contentPane.getChildren().addAll(parameterLabels.values());

        titlePane.getStyleClass().add(stylePrefix + "-header");
        contentPane.getStyleClass().add(stylePrefix + "-content");


        getChildren().add(titlePane);
        getChildren().add(contentPane);
    }

    public static String internalToString(Object ivalue){
        DecimalFormat df = new DecimalFormat("#.##");
        if(ivalue instanceof java.lang.Long){
            return df.format((Long) ivalue);
        }else if(ivalue instanceof java.lang.Double){
            return df.format((Double) ivalue);
        }else if(ivalue instanceof Range){
            String str = "(";
            Iterator it = ((Range) ivalue).iterator();
            while(it.hasNext()){
                str += internalToString(it.next()) + ",";
            }
            return str.substring(0,str.length()-1) + ")";
        }
        else if(ivalue instanceof clojure.lang.LazySeq){
            String str = "(";
            Iterator it = ((clojure.lang.LazySeq) ivalue).iterator();
            while(it.hasNext()){
                str += internalToString(it.next()) + ",";
            }
            return str.substring(0,str.length()-1) + ")";
        }
        else{
            return ivalue.toString();
        }
    }

    public int getNodeHeight(){
        return nodeHeight;
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
        if(!isRendered){
        }
        isRendered = true;
    }

    public double getStartX(){
        return getTranslateX();
    }
    public double getEndX(){
        return getTranslateX() + getWidth();
    }
    public double getCenterX(){
        return getTranslateX() + (getWidth() * 0.5);
    }
    public double getCenterY(){
        return getTranslateY() + (getHeight() * 0.5);
    }
    public double getStartY(){
        return getTranslateY();
    }
    public double getEndY(){
        return getTranslateY() + getHeight();
    }



    public void updateConnections(){
        for(CubicCurve l: inConnections){
                l.setEndX(getCenterX());
                l.setEndY(getStartY());
                l.setControlX2(getCenterX());
                l.setControlY2(getStartY()-150);
            }
        for(CubicCurve ol: outConnections){
            ol.setStartX(getCenterX());
            ol.setStartY(getEndY());
            ol.setControlX1(getCenterX());
            ol.setControlY1(getEndY()+150);
        }
    }

}
