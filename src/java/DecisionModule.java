import java.lang.Math;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import clojure.lang.Keyword;
import clojure.lang.PersistentArrayMap;

import hps.types.DecisionResult;
import hps.types.DecisionAlternative;
import hps.types.Expert;


public class DecisionModule extends Pane implements IConnectable {

    private static final double OFFSET = 40;
    public boolean wasAutoLayouted = false;
    private String name;

    private Rectangle bgrect;
    private ArrayList<GraphNode> producers = new ArrayList<GraphNode>();
    private ArrayList<GraphNode> evaluators = new ArrayList<GraphNode>();
    private ResultNode result;

    public ArrayList<CubicCurve> inConnections = new ArrayList<CubicCurve>();
    public ArrayList<CubicCurve> outConnections = new ArrayList<CubicCurve>();

    public DecisionModule(){ }
    public DecisionModule(String name, Object[] prods, Object[] evals){

        this.name = name;

        Label titleLabel = new Label(this.name);
        titleLabel.setTextFill(Color.WHITE);
        titleLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
        titleLabel.setTranslateX(5);
        titleLabel.setTranslateY(5);
        titleLabel.getStyleClass().add("decisionmodule-title");
        getChildren().add(titleLabel);

        for(int i = 0; i < prods.length; i++){
            Expert exp = (Expert) prods[i];
            Object params = exp.get(Keyword.intern("parameters"));
            String _name = ((Keyword) exp.get(Keyword.intern("name"))).getName();
            GraphNode node = new GraphNode(_name, NodeType.PRODUCER, params);

            producers.add(node);
            getChildren().add(node);
        }

        for(int i = 0; i < evals.length; i++){
            Expert exp = (Expert) evals[i];
            String _name = ((Keyword) exp.get(Keyword.intern("name"))).getName();
            Object params = exp.get(Keyword.intern("parameters"));
            GraphNode node = new GraphNode(_name, NodeType.EVALUATOR,params);

            evaluators.add(node);
            getChildren().add(node);
        }
        result = new ResultNode("Result",NodeType.RESULT);
        getChildren().add(result);

        bgrect = new Rectangle(0,0,100,100);
        bgrect.getStyleClass().add("decisionmodule");
        getChildren().add(bgrect);
        bgrect.toBack();
    }
    public double getStartX(){
        return getTranslateX();
    }
    public double getEndX(){
        return getTranslateX() + bgrect.getWidth();
    }
    public double getCenterX(){
        return getTranslateX() + (bgrect.getWidth() * 0.5);
    }
    public double getCenterY(){
        return getTranslateY() + (bgrect.getHeight() * 0.5);
    }
    public double getStartY(){
        return getTranslateY();
    }
    public double getEndY(){
        return getTranslateY() + bgrect.getHeight();
    }

    public void addInConnection(CubicCurve line){
        inConnections.add(line);
    }

    public void addOutConnection(CubicCurve line){
        outConnections.add(line);
    }

    private double vecLength(double x1, double y1, double x2, double y2){
        return Math.sqrt(Math.pow((x1-x2),2) + Math.pow((y1-y2),2));
    }

    public void updateConnections(){
        for(CubicCurve l: inConnections){
            if(vecLength(l.getStartX(), l.getStartY(), getStartX(), getCenterY())
               <= vecLength(l.getStartX(), l.getStartY(), getCenterX(), getStartY())){
                l.setEndX(getStartX());
                l.setEndY(getCenterY());
                l.setControlX2(getStartX()-150);
                l.setControlY2(getCenterY());
            }else{
                l.setEndX(getCenterX());
                l.setEndY(getStartY());
                l.setControlX2(getCenterX());
                l.setControlY2(getStartY()-150);
            }
        }

        for(CubicCurve ol: outConnections){
            if(vecLength(getEndX(), getCenterY(), ol.getEndX(), ol.getEndY())
               <= vecLength(getCenterX(), getEndY(), ol.getEndX(), ol.getEndY())){
                ol.setStartX(getEndX());
                ol.setStartY(getCenterY());
                ol.setControlX1(getEndX()+150);
                ol.setControlY1(getCenterY());
            }else{
                ol.setStartX(getCenterX());
                ol.setStartY(getEndY());
                ol.setControlX1(getCenterX());
                ol.setControlY1(getEndY()+150);
            }
        }
    }

    public void updateData(DecisionResult data){
        if(data == null){
            return;
        }else{
            if(!data.getClass().equals(DecisionAlternative.class)){
                System.out.println("Decision result is not an Alternative");
                return;
            }
            System.out.println(name + " " +  data.get(Keyword.intern("decision")));
            DecisionAlternative decision = (DecisionAlternative)(data.get(Keyword.intern("decision")));
            result.updateDataField(decision);
        }
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
        if(!isRendered){
            double pmaxw = 0;
            double pmaxh = OFFSET;
            for(GraphNode g: producers){
                pmaxw = Math.max(g.getWidth(), pmaxw);
                g.setTranslateY(pmaxh);
                pmaxh += g.getHeight() + OFFSET;
                g.setTranslateX(OFFSET);
            }
            double emaxw = 0;
            double emaxh = OFFSET;
            for(GraphNode g: evaluators){
                emaxw = Math.max(g.getWidth(), emaxw);
                g.setTranslateY(emaxh);
                emaxh += g.getHeight() + OFFSET;
                g.setTranslateX(OFFSET*2 + pmaxw);
            }
            double rw = result.getWidth();
            result.setTranslateY(OFFSET);
            result.setTranslateX(OFFSET*3 + pmaxw + emaxw);

            bgrect.setWidth(OFFSET * 4 + pmaxw + emaxw +rw);
            bgrect.setHeight(Math.max(Math.max(pmaxh,emaxh), result.getHeight() + OFFSET*2));
        }
        isRendered = true;
        updateConnections();
    }
}
