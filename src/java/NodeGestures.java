
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.input.MouseEvent;

class NodeGestures {
    private DragContext nodeDragContext = new DragContext();

    DotGrid canvas;

    public NodeGestures( DotGrid canvas) {
        this.canvas = canvas;

    }

    public EventHandler<MouseEvent> getOnMousePressedEventHandler() {
        return onMousePressedEventHandler;
    }

    public EventHandler<MouseEvent> getOnMouseDraggedEventHandler() {
        return onMouseDraggedEventHandler;
    }

    private EventHandler<MouseEvent> onMousePressedEventHandler = new EventHandler<MouseEvent>() {

        public void handle(MouseEvent event) {
            if( !event.isPrimaryButtonDown())
                return;

            nodeDragContext.mouseAnchorX = event.getSceneX();
            nodeDragContext.mouseAnchorY = event.getSceneY();

            Pane node = (Pane) event.getSource();

            nodeDragContext.translateAnchorX = node.getTranslateX();
            nodeDragContext.translateAnchorY = node.getTranslateY();
        }

    };

    private EventHandler<MouseEvent> onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
        public void handle(MouseEvent event) {

            if( !event.isPrimaryButtonDown())
                return;

            double scaleX = canvas.getScaleX();
            double scaleY = canvas.getScaleY();

            Pane node = (Pane) event.getSource();


            ((IConnectable)node).updateConnections();
            node.setTranslateX(nodeDragContext.translateAnchorX +
                               (( event.getSceneX() - nodeDragContext.mouseAnchorX) / scaleX));
            node.setTranslateY(nodeDragContext.translateAnchorY +
                               (( event.getSceneY() - nodeDragContext.mouseAnchorY) / scaleY));


            event.consume();

        }
    };
}
