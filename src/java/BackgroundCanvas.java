
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

import javafx.scene.shape.Line;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.layout.Pane;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import hps.types.DecisionResult;

import clojure.lang.Keyword;
import clojure.lang.PersistentArrayMap;

public class BackgroundCanvas extends Pane{

    Scene myScene;
    SceneGestures gestures;
    NodeGestures nodeGestures;
    DotGrid content;
    HashMap<String,DecisionModule> decisionModules;
    HashMap<String,GraphNode> otherNodes;

    Button saveButton;
    Button loadButton;

    public BackgroundCanvas (int width, int height){
        setMinWidth(width);
        setMinHeight(height);
        getStyleClass().add("background-canvas");
        content = new DotGrid();

        content.setMinWidth(width);
        content.setMinHeight(height);

        getChildren().add(content);
        gestures = new SceneGestures(content);
        nodeGestures = new NodeGestures(content);
        decisionModules = new HashMap<String, DecisionModule>();
        otherNodes = new HashMap<String, GraphNode>();

        saveButton = new Button("Save");
        loadButton = new Button("Load");
        getChildren().add(saveButton);
        getChildren().add(loadButton);
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    try{
                    PrintWriter writer = new PrintWriter(".graphpositions.txt", "UTF-8");
                    for(String key: decisionModules.keySet()){
                        DecisionModule dm = decisionModules.get(key);
                        writer.println(key + "," + (int)dm.getTranslateX() + "," + (int)dm.getTranslateY());
                    }
                    writer.println("");
                    for(String key: otherNodes.keySet()){
                        GraphNode node = otherNodes.get(key);
                        writer.println(key + "," + (int)node.getTranslateX() + "," + (int)node.getTranslateY());
                    }
                    writer.close();
                    }catch(Exception ex){
                        System.out.println(ex);
                    }
                    // System.out.println("Saved graph node layout");
                }
            });

        loadButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override public void handle(ActionEvent e){
                    try{
                        Scanner scanner = new Scanner(new File(".graphpositions.txt"));
                        scanner.useDelimiter("\n");
                        while(scanner.hasNext()){
                           String[] values = scanner.next().split(",");
                            if(values.length < 3) break;
                            String key = values[0];
                            float x = Float.parseFloat(values[1]);
                            float y = Float.parseFloat(values[2]);
                            if(decisionModules.get(key) != null){
                                decisionModules.get(key).setTranslateX(x);
                                decisionModules.get(key).setTranslateY(y);
                            }
                        }
                        while(scanner.hasNext()){
                            String[] values = scanner.next().split(",");
                            String key = values[0];
                            float x = Float.parseFloat(values[1]);
                            float y = Float.parseFloat(values[2]);
                            if(otherNodes.get(key) != null){
                                otherNodes.get(key).setTranslateX(x);
                                otherNodes.get(key).setTranslateY(y);
                            }
                        }
                        scanner.close();
                    }catch(Exception ex){
                        System.out.println(ex);
                    }
                    // System.out.println("Loaded graph node layout");
                }
            });
    }

    public void addGenericNode (String name, Object fields){
        System.out.println(" Add " + name + " with " + fields);
        GraphNode node = content.addGenericNode(name, fields);
        otherNodes.put(name, node);

        node.addEventFilter(MouseEvent.MOUSE_PRESSED, nodeGestures.getOnMousePressedEventHandler());
        node.addEventFilter(MouseEvent.MOUSE_DRAGGED, nodeGestures.getOnMouseDraggedEventHandler());
    }

    public void addDecisionModule(hps.types.DecisionModule dm){
        String name = ((Keyword)dm.get(Keyword.intern("name"))).getName();
        DecisionModule mod = content.addDecisionModule(dm);
        decisionModules.put(name,mod);

        mod.addEventFilter(MouseEvent.MOUSE_PRESSED, nodeGestures.getOnMousePressedEventHandler());
        mod.addEventFilter(MouseEvent.MOUSE_DRAGGED, nodeGestures.getOnMouseDraggedEventHandler());
    }


    public void connectDecisionModules(ArrayList<Keyword> from, ArrayList<Keyword> to){
        for(int i = 0; i < from.size(); i++){
            content.connectDecisionModules(decisionModules.get(from.get(i).getName()),
                                           decisionModules.get(to.get(i).getName()));
        }
    }


    public void updateGraphData(Object dmname, Object newvalue ){
        decisionModules.get(dmname).updateData((DecisionResult)newvalue);
    }

    public void setScene(Scene scene){
        myScene = scene;
        myScene.addEventFilter(MouseEvent.MOUSE_PRESSED, gestures.getOnMousePressedEventHandler());
        myScene.addEventFilter(MouseEvent.MOUSE_DRAGGED, gestures.getOnMouseDraggedEventHandler());
        myScene.addEventFilter(ScrollEvent.ANY, gestures.getOnScrollEventHandler());
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
        if(!isRendered){
            for(String key: decisionModules.keySet()){
                decisionModules.get(key).layoutChildren();
            }
        }
        loadButton.setTranslateY(getHeight() - 30);
        saveButton.setTranslateX(getWidth() - 50);
        saveButton.setTranslateY(getHeight() - 30);
    }
}
